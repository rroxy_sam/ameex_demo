/**
     Module : UserController,
     Description : "Contains User Serach Functionality"
*/

let UserModel = require(APP_PATH + '/api/models/UserModel.js');

class UserController {

     /** Used to serach RELATED users */
     search (req, res) {
          let conditions = {};

          /** Checking skills query*/
          if(req.query.skills) {
               conditions['skills'] = {
                    $elemMatch : {
                                   $regex : req.query.skills, $options : 'i'
                              }
               }
          }

          /** Checking user related query*/
          if(req.query.name) {

               conditions['$or'] = [
                    {
                         "username" : {

                              $regex : req.query.name, $options : 'i'
                         }
                    },
                    {
                         "name.first" : {

                              $regex : req.query.name, $options : 'i'
                         }
                    },
                    {
                         "name.last" : {

                              $regex : req.query.name, $options : 'i'
                         }
                    },
                    {
                         "email" : {

                              $regex : req.query.name, $options : 'i'
                         }
                    }

               ]

          }

          /** Retrieving User info on basis of search query */
          UserModel
          .find (conditions)
          .exec (function (err, data) {
               if(err) return res.json({ status: 'error', message:'Error occured while retriving data' });
               return res.json({
                    status  : 'success',
                    message : 'Data lIST',
                    result : data
               });
          });
     }

}

module.exports = new UserController(UserModel);
