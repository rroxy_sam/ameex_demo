/*
* Module : UserModel
* Description : Contains the users schema
*/
let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let ObjectId = Schema.ObjectId;

let modelSchema = new Schema({
     name : {
          first: { type: String},
          last: {type: String}
     },
     username : {
          type : String
     },
     email : {
          type : String
     },
     skills : [

     ],
     status : {
          type : Boolean,
          enum : [true, false],
          default :false
     },
     isDeleted : {
          type : Boolean,
          enum : [true, false],
          default : false
     }
},{
      timestamps: true,
      collection:'users'
}
);

/** Virtual field for full name */
modelSchema.virtual('fullname')
.get(function () {
     return this.name.first + ' ' + this.name.last;
});

let modelObj = mongoose.model('User', modelSchema);
module.exports = modelObj;
