/*
* Author : Sunny Chauhan
* Module : UserRoute
* Description : Contains User related serach Route
*/

module.exports = function(app, router) {

     // requiring User Controller
     let UserController = require(APP_PATH + '/api/controllers/UserController.js');
     
     router.get('/search', UserController.search);
}
