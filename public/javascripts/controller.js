/** Search Controller */

function SearchCtrl($scope,$state,SearchService) {
     $scope.search = function(Obj){

          if(Obj.name || Obj.skills){
               //alert(Obj.name);
               SearchService.list(Obj,function(data){
                    if(data.status =="success"){
                         $scope.dataList = data.result;

                    }
               })
          }
     }
};

/* Defining Implode filter */
function implode() {
     return function(data){

          return data.toString();
     }
};

angular
.module('ameex')
.controller ('SearchCtrl', SearchCtrl)
.filter ('implode', implode)
