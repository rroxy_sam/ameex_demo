/** Search Service for interacting with server */

function SearchService ( $http,BASE_PATH ) {
     return {
          list : function(Obj,cb) {
               Obj.name=Obj.name|| "";
               Obj.skills=Obj.skills|| "";
               $http.get(BASE_PATH+'search?name='+Obj.name+'&skills='+Obj.skills).then(function(result){
                    cb(result.data);
               })
          }
     }
}

angular
.module('ameex')
.service('SearchService', SearchService)
