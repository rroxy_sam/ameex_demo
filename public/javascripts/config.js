

function config($stateProvider, $urlRouterProvider, BASE_PATH) {

     $urlRouterProvider.otherwise("/");

     $stateProvider
     .state('search', {
          url: "/",
          templateUrl: "/templates/search.html",
          controller : 'SearchCtrl'
     })
}

angular
.module('ameex')
.config(config);
